Feature: Hello world
    The first application a developer starts with is usually hello world. The idea is to start by
    simply returning an output of "Hello world" and build a bit from there.


    Scenario: Showing "Hello world"
        When I click the action button
        Then I should see 'Hello world' in the output

    Scenario: Using a name
        Given I entered 'Wyart' in the input 
        When I click the action button
        Then I should see 'Hello Wyart' in the output

    Scenario: Using no name
        Given I entered nothing in the input
        When I click the action button
        Then I should see 'Hello world' in the output
